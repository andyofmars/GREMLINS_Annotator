function [DAT,f] = pip_stats(proxy,int,fig_en)
% Extracts metrics of SANDPIPER PIP for each PIP
% peak_P | mean_P | rise_time | fall_time
% proxy table of pressure values and times
% int table of interventions, time, type and value

% extract only the 'Sol_LVL' events
int = int(int.Event == 'Sol_LVL',:);


event_data = struct('mean_base', [], 'peak', [], 'peak_time', [], 'rise_time', [], 'fall_time', [], 'mean_peak', [], 'pulse_width', []);
DAT = event_data;

f = [];

% iterate over each pip pair (on-off)
% check that it starts with a on pip
if size(int,1) > 0
    if int.Value(1) == 0
        int = int(2:end,:);
    end

    for i=1:floor(size(int.Value)/2)
        clear event_data
        % extract time values
        event_times = [int.RelTimes(2*i-1) int.RelTimes(2*i)];

        % extract the section of data we want - -10mins to event start
        baseline_p_data = proxy(proxy.RelTime < event_times(1) & proxy.RelTime >= 0 & proxy.RelTime >= event_times(1) - 600,:);
        % adjust time to include end of event (1.5 times true length)
        event_p_data = proxy(proxy.RelTime >= event_times(1) & proxy.RelTime <= event_times(2)+0.5*(event_times(2)-event_times(1)),:);

        N = length(event_p_data.RelTime);
        
        if N >3
            % smooth the waveform
            event_p_data.Pressure = sgolayfilt(event_p_data.Pressure,2,23);

            % Get baseline pressure from basline_p_data
            event_data.mean_base = mean(baseline_p_data.Pressure);

            % Peak pressure
            [pk, loc] = findpeaks(event_p_data.Pressure);
            [m, I] = max(pk);
            pk = m;
            loc = loc(I);
            event_data.peak = pk;
            event_data.peak_time = event_p_data.RelTime(loc) - event_p_data.RelTime(1);
            
            rise_idx = NaN;
            fall_idx = NaN;

            % find 50% rise time and fall time
            j = 1;
            a = 1/(exp(1));
            while (event_p_data.Pressure(j) < a*(event_data.peak - event_data.mean_base) + event_data.mean_base)
                j = j+1;
                if j>N
                    break;
                end
            end
            if j>N
                event_data.rise_time = NaN;
                rise_idx = NaN;
            else
                event_data.rise_time = event_p_data.RelTime(j) - event_p_data.RelTime(1);
                rise_idx = j;
            end

            j = loc;
            a = 1/exp(1);
            while (event_p_data.Pressure(j) > a*(event_data.peak - event_data.mean_base) + event_data.mean_base)
                j = j+1;
                if j>N
                    break;
                end
            end
            if j>N
                event_data.fall_time = NaN;
                fall_idx = NaN;
            else
                event_data.fall_time = event_p_data.RelTime(j) - event_p_data.RelTime(loc);
                fall_idx = j;
            end
            
            display(rise_idx);
            display(fall_idx);
            
            rise_idx
            fall_idx

            % extract mean pressure over PIP    
            if isnan(fall_idx) || isnan(rise_idx)
                event_data.mean_peak = NaN;
            else
                event_data.mean_peak = mean(event_p_data.Pressure(rise_idx:fall_idx));
            end

            % extract pulse width
            event_data.pulse_width = event_data.fall_time + event_data.peak_time - event_data.rise_time;

            if fig_en
                f(i) = figure;
                set(f(i),'visible','off')
                plot([baseline_p_data.RelTime([end-100:end]); event_p_data.RelTime]-event_p_data.RelTime(1),[baseline_p_data.Pressure([end-100:end]); event_p_data.Pressure])
                hold on
                plot(event_data.peak_time,event_data.peak,'rx')
                if ~isnan(rise_idx)
                    plot(event_data.rise_time,event_p_data.Pressure(rise_idx),'go')
                end
                if ~isnan(fall_idx)
                    plot(event_times(2)+event_data.fall_time-event_p_data.RelTime(1),event_p_data.Pressure(fall_idx),'go')
                end
                plot([event_data.rise_time event_data.peak_time+event_data.fall_time],event_data.mean_peak*ones(1,2),'-k')
                plot(baseline_p_data.RelTime([end-100 end])-event_p_data.RelTime(1),event_data.mean_base*ones(1,2),'-k')
                yz = get(gca,'Ylim');
                plot(event_times(1)*ones(1,2)-event_p_data.RelTime(1),yz,'--k')
                plot(event_times(2)*ones(1,2)-event_p_data.RelTime(1),yz,'--k')
                ylim(yz)
                hold off
            end
        else
            event_data.mean_base = NaN;
            event_data.peak = NaN;
            event_data.peak_time = NaN;
            event_data.rise_time = NaN;
            event_data.fall_time = NaN;
            event_data.mean_peak = NaN;
            event_data.pulse_width = NaN;
        end
        
        DAT(i,:) = event_data;
    end
end
end


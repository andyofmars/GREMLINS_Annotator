function [RA_amplitude,RA_pressure] = process_cpap(dat,times)
% Fuction to calculate a rolling mean of the CPAP level (RA_pressure) and the bubble amplitude (RA_amplitude)
% %   Detailed explanation goes here
% outputArg1 = inputArg1;
% outputArg2 = inputArg2;


   %% Filter data
   f_cut = 25;
   f_sample = 100; %%%%%%%%%%%%%%%%%%%%%%%%
   order = 8;
   [z,p,k] = butter(order,f_cut/f_sample,'low');
   sos = zp2sos(z,p,k);
   filtered_dat = sosfilt(sos,dat(:,2));
     
   %% Calculate CPAP level
   % Set thresholds and windows
   window_mean = 100 %%%%%%%%%%%%%%%%%%%%%%%%                                        % Rolling average window for mean cpap presure, in 1s/100
   
   % Rolling average CPAP level
   RA_pressure = zeros(1,length(times));
   
   N = length(times);
   N2 = size(dat,1)-1;
   RA_pressure(1) = NaN;
   for i=2:N
       %RA_pressure(i) = mean(filtered_dat(dat(2:end,1)<times(i) & dat(2:end,1)>times(i) - window_mean));
       idx = [window_mean*(i-2)+1:window_mean*(i-1)];
       if idx(end) > N2
           idx = idx(idx<N2);
       end 
       RA_pressure(i) = mean(filtered_dat(idx));
   end
   weird_idxs = find(RA_pressure<0); 
   RA_pressure(weird_idxs) = NaN;

   %% Calculate CPAP amplitude
   % Set thresholds and windows
   window_thres = 100  %%%%%%%%%%%%%%%%%%%%%%%%                                       % Window of amplitudes on which the threshold is based, in # of samples
   rel_thres = 0.30;                                        % Relative threshold of amplitude, in %
%   t_diff = 50;                                             % Max difference in time between two maxima/minima, in s/100
   window_ampli = 1;                                       % Rolling average window for the amplitude, in s
    
   % Find all peaks and troughs
   [all_peaks, all_peak_locs]= findpeaks (filtered_dat(2:end));
   [all_troughs, all_trough_locs]= findpeaks (-filtered_dat(2:end));

   % Combine into one list and sort
   peaks = [all_peak_locs, all_peaks, ones(length(all_peak_locs),1)];
   troughs = [all_trough_locs, -all_troughs, zeros(length(all_trough_locs),1)];
   combined = [peaks; troughs];
   [~,I] = sort(combined(:,1),1);
   sorted = combined(I,:);

   % Intialisation of threshold
   abs_max = mean(abs(diff(sorted(sorted(:,1)<window_thres,2))))/2;
   all_thresholds = [];

   % Set all parameters
   loop_count = 0;
   output = zeros(size(sorted));
   amplitudes = [];
   i = 1;                                                   % Start point
   ii = 1;
   N = size(sorted,1);
   
   % Define threshold
   while (i<N)                                                                                      
        if size(amplitudes,1)<2                             % If there are no peaks/troughs saved (at start)
            thresh = rel_thres*abs_max;                     % Define threshold based on first peaks/trough within window
        else                                                % If new peaks/troughs are saved and amplitude is found
            amplitudes = amplitudes(amplitudes(:,2) > sorted(i,1) - window_thres,:);    % Find all amplitudes in new window
            if size(amplitudes,1)>1
               thresh = rel_thres * median(amplitudes(:,1));                            % Take median amplitude in window
            end
        end
        % Evaluate peaks and troughs
        CONTINUE = 1;
        j = 1;
        k = 1;
        n1 = 1;
        n2 = 1;
        while (CONTINUE)
            if i+j > N || i+2*n2 > N || i+2*n1 > N          % If next point is the end of the file
                i=N;                                        % Stop the loop
                CONTINUE = 0;
            else
                 % If distance between i and i+j is above threshold
                 if (sorted(i+j,3) == 1 && (sorted(i+j,2) - sorted(i,2)) > thresh && sorted(i,3) == 0) || (sorted(i+j,3) == 0 && (sorted(i,2) - sorted(i+j,2)) > thresh && sorted(i,3) == 1)
                     CONTINUE = 0;
                     output(ii,:) = sorted(i,:);            % Save this peak/trough
                     i = i+j;                               % Move to i+j                           
                 % If distance between i and i+j is below threshold
                 else
                     if sorted(i,3)==1                      % If i is a peak
                         if sorted(i+2*n1,2) > sorted(i,2)  % If next peak is higher
                            i=i+2;                          % Move to the next peak
                            j=1;                            % Reset other values
                            n1=1;
                         else                               % If next peak is lower
                            j = j+2;                        % Check next trough but stay in i
                            n1=n1+1;                        % If loop is entered again, compare height with next peak
                         end
                     else                                   % If i is a trough
                         if sorted(i+2*n2,2) < sorted(i,2)  % If next trough is lower
                            i = i+2;                        % Move to next trough
                            j=1;                            % Reset other values
                            n2=1;
                         else                               % If next trough is higher
                            j = j+2;                        % Check next peak but stay in i
                            n2=n2+1;                        % If loop is entered again, compare height with next trough
                         end
                     end
                 end
            end
        end
        
        if i<N && ii>1
             if isempty(amplitudes)
                amplitudes = [abs(output(ii,2) - output(ii-1,2))/2, (output(ii,1) - output(ii-1,1))/2 + output(ii-1,1)];
            else
                amplitudes = [abs(output(ii,2) - output(ii-1,2))/2, (output(ii,1) - output(ii-1,1))/2 + output(ii-1,1);...
                          amplitudes];
             end
        end
        ii = ii+1;
    end

   
   % Remove zeros at end of output
   output = output(output(:,1)~=0,:);
        
   % Find amplitudes
   all_amplitudes = abs(diff(output(:,2)));
   samples = round(diff(output(:,1))/2 + output(1:end-1,1));
   all_amplitudes = all_amplitudes/2;
   sample_times = dat(samples,1);

   % Exclude ampltiudes where time between point is larger than t_diff
%    idx = find([diff(samples)' 0]<t_diff);
%    all_amplitudes = all_amplitudes(idx);
%    samples = samples(idx);
%    sample_times = sample_times(idx);

   % Rolling average CPAP amplitudes
   RA_amplitude = zeros(size(times,1),1);
   for i=1:length(times)
       RA_amplitude(i) = mean(all_amplitudes(sample_times<times(i) & sample_times>=times(i) - window_ampli));
   end
   RA_amplitude = RA_amplitude(1:i);
   RA_amplitude(isnan(RA_amplitude)) = 0;
   
   RA_amplitude(weird_idxs) = NaN;
end

